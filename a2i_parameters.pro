; docformat = 'rst'
;
;+
;
; :Purpose:
;
; :Inputs:
;   case_name: (String) simulation case name
;   
; :Requires:
;   case parameters file: {INPUT_DIRECTORY}/a2i_parameters_{case_name}.csv
;   pollutant parameters file: {INPUT_DIRECTORY}/a2i_pollutant_parameters{optional extension in case parameters file}.csv
;
; :Outputs:
;   Parameters hash table
;
; :Example::
;   parameters=a2i_parameters('CFC')
;   
; :History:
; 	Written by: Matt Rigby, MIT, Aug 1, 2012
;
;-
function a2i_parameters, case_name

  compile_opt idl2, hidden

  parameters=hash()
  parameters['CASE_NAME']=case_name

  ;CASE PARAMETERS
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  
  fname=a2i_filestr(/input, case_name + '/parameters.csv')
  if fname eq '' then begin
    message, "CAN'T FIND PARAMETER FILE, ", CASE_NAME
  endif

  openr, fun, fname, /get_lun
    while not eof(fun) do begin
      str=''
      readf, fun, str
      str=strSplit(str, ',', /extract, /preserve_null)
      rowHeader=strTrim(strUpCase(str[0]), 2)
      str=str[1:*]
      case strTrim(strUpcase(rowHeader), 2) of
        'POLLUTANTS': parameters['POLLUTANTS']=str[where(str ne '')]
        'START YEAR': parameters['STARTY']=fix(str[0])
        'END YEAR': parameters['ENDY']=fix(str[0])
        'ESTIMATE TRANSPORT?': parameters['TRANSPORT_ESTIMATE']=(strUpCase(str[0]) eq 'Y')
        'ESTIMATE OH?': parameters['OH_ESTIMATE']=(strUpCase(str[0]) eq 'Y')
        'INTER-ANNUALLY VARYING TRANSPORT': parameters['IAV']=(strUpCase(str[0]) eq 'Y')
        'TRANSPORT PARAMETER FILE': parameters['TRANSPORT_FILE']=str[0]
        'OH FILE': parameters['OH_FILE']=str[0]
        'TRANSPORT GROWTH CONSTRAINT?': parameters['T_GROWTH']=(strUpCase(str[0]) eq 'Y')
        'OH GROWTH CONSTRAINT?': parameters['OH_GROWTH']=(strUpCase(str[0]) eq 'Y')
;        'FUNCTION: MOLE FRACTIONS': parameters['FUNCTION_Y']=strUpCase(str[0])
;        'FUNCTION: INITIAL CONDITIONS': parameters['FUNCTION_IC']=strUpCase(str[0])
;        'FUNCTION: EMISSIONS': parameters['FUNCTION_Q']=StrUpCase(str[0])
        'FUNCTION: STRATOSPHERIC LIFETIME': parameters['FUNCTION_S']=strUpCase(str[0])
        'FUNCTION: OH': parameters['FUNCTION_OH']=StrUpCAse(str[0])
        'FUNCTION: T^(-1)': parameters['FUNCTION_T']=strUpCase(str[0])
        'TRANSPORT PARAMETER ERROR (%)': parameters['ERROR_T']=float(str[0])/100.
        'OH ERROR (%)': parameters['ERROR_OH']=float(str[where(str ne '')])/100.
        'MEASUREMENT FOLDER': parameters['MEASUREMENT_FOLDER']=str[0]
        'POLLUTANT FILE NAME SUFFIX': pollutant_file_extension=str[0]
        'INCLUDE ARCHIVE?': parameters['ARCHIVE']=(strUpCase(str[0]) eq 'Y')
        'INCLUDE FIRN?': parameters['FIRN']=(strUpCase(str[0]) eq 'Y')
        else: ;Do nothing
      endcase
    endwhile
  close, fun
  free_lun, fun

  if parameters['OH_ESTIMATE'] eq 0 then begin
    parameters['OH_GROWTH']=0
  endif
  if parameters['TRANSPORT_ESTIMATE'] eq 0 then begin
    parameters['T_GROWTH']=0
  endif
  if ~parameters.haskey('FIRN') then begin
    parameters['FIRN']=0
  endif

  ;POLLUTANT PARAMETERS
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  
  nPollutants=n_elements(parameters['POLLUTANTS'])
  if n_elements(pollutant_file_extension) eq 0 then begin
    pollutant_file_extension=''
  endif else begin
    if pollutant_file_extension ne '' then begin
      pollutant_file_extension='_' + pollutant_file_extension
    endif
  endelse

  fname=a2i_filestr(/input, 'a2i_pollutant_parameters' + pollutant_file_extension + '.csv')
  if fname eq '' then begin
    message, "CAN'T FIND POLLUTANT PARAMETER FILE, ", FNAME
  endif
  
  openr, fun, fname, /get_lun

    header=''
    readf, fun, header
    header=strSplit(header, ',', /extract)
    pollutants_parameters=header[where(header ne '' and header ne ' ')]

    wh_parameters=intarr(nPollutants)
    for pi=0, nPollutants-1 do wh_parameters[pi]=where(pollutants_parameters eq (parameters['POLLUTANTS'])[pi] )

    while not eof(fun) do begin
      str=''
      readf, fun, str
      str=strSplit(str, ',', /extract, /preserve_null)
      rowHeader=strTrim(strUpCase(str[0]), 2)
      str=str[1:*]
      case rowHeader of
        'GROUP': parameters['GROUP']=str[wh_parameters]
        'POLLUTANT STRING': parameters['POLLUTANT_STRING']=str[wh_parameters]
        'MOLECULAR MASS (G/MOL)': parameters['MOL_MASS']=str[wh_parameters]
        'AGAGE SYSTEM': parameters['AGAGE_SYSTEM']=str[wh_parameters]
        'UNIT': parameters['UNIT']=str[wh_parameters]
        'RADIATIVE EFFICIENCY (W/m2/ppb)': parameters['UNIT']=str[wh_parameters]
        'ALE/GAGE': parameters['ALE']=(str[wh_parameters] eq 'Y')
        'START YEAR': parameters['POLLUTANT_START_YEAR']=fix(str[wh_parameters])
        
        'MOLE FRACTION FUNCTION': parameters['FUNCTION_Y']=StrUpCase(str[wh_parameters])
        
        'SCALE ERROR PLUS (%)': parameters['SCALE_ERROR_PLUS']=float(str[wh_parameters])/100.
        'SCALE ERROR MINUS (%)': parameters['SCALE_ERROR_MINUS']=float(str[wh_parameters])/100.
        'OVERALL LIFETIME UNCERTANTY (%)': parameters['GLOBAL_LIFETIME_ERROR']=float(str[wh_parameters])/100.

        'STRATOSPHERIC LIFETIME (YEARS)': parameters['STRATOSPHERIC_LIFETIME']=float(str[wh_parameters])
        'STRATOSPHERIC LIFETIME ERROR (%)': parameters['STRATOSPHERIC_LIFETIME_ERROR']=float(str[wh_parameters])/100.
        'STRATOSPHERIC LIFETIME FREQUENCY (YEARS)': parameters['STRATOSPHERIC_LIFETIME_FREQUENCY']=float(str[wh_parameters])
        'ESTIMATE STRATOSPHERE?': parameters['STRATOSPHERE_ESTIMATE']=(strUpCase(str[wh_parameters]) eq 'Y')

        'TROPOSPHERIC LIFETIME (YEARS)': parameters['TROPOSPHERIC_LIFETIME']=float(str[wh_parameters])
        'OCEAN LIFETIME (YEARS)': parameters['OCEAN_LIFETIME']=float(str[wh_parameters])

        'OH_A': parameters['OH_A']=float(str[wh_parameters])
        'OH_ER': parameters['OH_ER']=float(str[wh_parameters])
        'OH_F': parameters['OH_F']=float(str[wh_parameters])
        'OH_G': parameters['OH_G']=float(str[wh_parameters])
        
        'EMISSIONS FUNCTION': parameters['FUNCTION_Q']=strUpCase(str[wh_parameters])
        'EMISSIONS ERROR (%)': parameters['EMISSIONS_ERROR']=float(str[wh_parameters])/100.
        'EMISSIONS BIAS ERROR (%)': parameters['EMISSIONS_BIAS_ERROR']=float(str[wh_parameters])/100.
        'EMISSIONS MINIMUM ERROR (%)': parameters['EMISSIONS_MINIMUM_ERROR']=float(str[wh_parameters])/100.
        'EMISSIONS FREQUENCY (PER YEAR)': parameters['EMISSIONS_FREQUENCY']=fix(str[wh_parameters])
        'ESTIMATE EMISSIONS?': parameters['EMISSIONS_ESTIMATE']=(strUpCase(str[wh_parameters]) eq 'Y')

        'EMISSIONS GROWTH ERROR (%)': parameters['EMISSIONS_GROWTH_ERROR']=float(str[wh_parameters])/100.
        'EMISSIONS MINIMUM GROWTH ERROR (%)': parameters['EMISSIONS_MINIMUM_GROWTH_ERROR']=float(str[wh_parameters])/100.
        'EMISSIONS LATITUDIAL GROWTH ERROR (%)': parameters['EMISSIONS_LATITUDINAL_GROWTH_ERROR']=float(str[wh_parameters])/100.
        'EMISSIONS GROWTH CONSTRAINT?': parameters['EMISSIONS_GROWTH']=(strUpCase(str[wh_parameters]) eq 'Y')

        'INITIAL CONDITIONS GROWTH CONSTRAINT?': parameters['IC_GROWTH']=(strUpCase(str[wh_parameters]) eq 'Y')
        'INITIAL MOLE FRACTION (PPT APPROX)': parameters['IC_APPROX']=float(str[wh_parameters])
        'INITIAL CONDITIONS ERROR (%)': parameters['ERROR_IC']=float(str[wh_parameters])/100.

        'REPEATABILITY (%)': parameters['MEASUREMENT_REPEATABILITY']=str[wh_parameters] ;THIS IS A STRING (see measurements)
        'REPEATABILITY YEAR': parameters['MEASUREMENT_REPEATABILITY_YEAR']=str[wh_parameters] ;THIS IS A STRING (see measurements)
        'MEASUREMENTS TO REMOVE (STATION(MONTH/DAY/YEAR TO MONTH/DAY/YEAR))': $
          parameters['MEASUREMENT_REMOVE']=str[wh_parameters]
        'DETECTION LIMIT': parameters['MEASUREMENT_DETECTION_LIMIT']=float(str[wh_parameters])

        else: ;Do nothing
      endcase
    endwhile

  close, fun
  free_lun, fun

  parameters['NPOLLUTANTS']=n_elements(parameters['POLLUTANTS'])
  parameters['TIMESIZE']=(parameters['ENDY'] - parameters['STARTY'])*12L
  parameters['NYEARS']=(parameters['ENDY'] - parameters['STARTY'])

  ;Initial conditions function
  function_IC=strarr(parameters['NPOLLUTANTS'])
  for pi=0, parameters['NPOLLUTANTS']-1 do begin
    case parameters['FUNCTION_Y', pi] of
      'Y': function_IC[pi]='X'
      'LN(Y)': function_IC[pi]='LN(X)'
    endcase
  endfor
  parameters['FUNCTION_IC']=function_IC

  return, parameters

end