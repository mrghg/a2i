pro a2i_setup, case_name, force=force

  if n_elements(case_name) eq 0 then begin
    print, "AIF_RUN: CASE_NAME needed"
    return
  endif
  
  dirname=a2i_filestr(/Input, case_name)
  dirExists=(file_search(dirname) ne '')
  
  if dirExists and ~keyword_set(force) then begin
    print, 'DIRECTORY ' + case_name + ' EXISTS, EXITING!'
    return
  endif else begin
    response=''
    read, "Create directory " + dirName + " and subdirectories (Y/N)?  ", response
    if strupcase(response) eq 'Y' then begin
      ;Make a main directory
      file_mkdir, dirname
      ;Make subdirectories
      file_mkdir, strcompress(dirname + '/output', /remove_all)
      file_mkdir, strcompress(dirname + '/output/emissions', /remove_all)
      file_mkdir, strcompress(dirname + '/output/mf', /remove_all)
      file_mkdir, strcompress(dirname + '/emissions', /remove_all)
      file_mkdir, strcompress(dirname + '/measurements', /remove_all)
      file_copy, file_which('a2i_parameters_template.csv'), strcompress(dirname + '/parameters.csv', /remove_all)
      ;Make output directory
      file_mkdir, a2i_filestr(/Output, case_name)
      file_mkdir, a2i_filestr(/Output, case_name + '/emissions')
      file_mkdir, a2i_filestr(/Output, case_name + '/mf')
    endif else begin
      print, 'EXITING'
      return
    endelse
  endelse
  
end