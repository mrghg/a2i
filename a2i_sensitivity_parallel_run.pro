pro a2i_sensitivity_parallel_run, parameters, y, model, x, lifetime, restart, xi_start, xi_end, $
  ;Output keywords
  H, H_lifetime, error_check

  compile_opt idl2, hidden

  error_check='no probs'

  Catch, theError
  IF theError NE 0 THEN BEGIN
    Catch, /Cancel
    Help, /Last_Message, Output=theErrorMessage
    error_Check=''
    FOR j=0,N_Elements(theErrorMessage)-1 DO BEGIN
      error_Check=error_Check + theErrorMessage[j]
    ENDFOR
    return
  ENDIF


  nStateSub=xi_end - xi_start + 1
  H=fltarr(nStateSub, y['NMEASUREMENTS'])
  H_lifetime=fltarr(nStateSub, parameters['TIMESIZE']*parameters['NPOLLUTANTS'])

  for xi=xi_start, xi_end do begin
    
    if X['POLLUTANT', xi] ge 0 then begin
      pi=X['POLLUTANT', xi]
    endif else begin
      pi=!null
    endelse
  
    x_pert=mr_hash_copy(x)
    x_pert['X', xi]=x['X', xi] + x['PERTSIZE', xi]
    model_pert=a2i_model_scale(parameters, y, model, x_pert)

    y_pert=a2i_model_run(parameters, y, model_pert, lifetime, in_restart=restart, $
      pollutant=pi, startT=X['PERTSTART', xi], endT=X['PERTEND', xi], out_lifetime=out_lifetime, $
      x=x, state_element=xi)
  
    if x['POLLUTANT', xi] ge 0 then begin
      wh_y=where(y['POLLUTANT'] eq x['POLLUTANT', xi] and $
        y['TI'] ge x['PERTSTART', xi] and $
        y['TI'] le x['PERTEND', xi], count)
      wh_lifetime=where(lifetime['POLLUTANT'] eq x['POLLUTANT', xi] and $
        lifetime['TI'] ge x['PERTSTART', xi] and $
        lifetime['TI'] le x['PERTEND', xi], count)
    endif else begin
      wh_y=where(y['TI'] ge x['PERTSTART', xi] and y['TI'] le x['PERTEND', xi], count)
      wh_lifetime=where(lifetime['TI'] ge x['PERTSTART', xi] and $
        lifetime['TI'] le x['PERTEND', xi], count)
    endelse
    
;    if count eq 0 then stop
;    H[xi - xi_start, wh_y]=(y_pert[wh_y] - y['MODEL', wh_y])/x['PERTSIZE', xi]
;    H_lifetime[xi - xi_start, wh_lifetime]=(out_lifetime[wh_lifetime] - lifetime['MODEL', wh_lifetime])/x['PERTSIZE', xi]

    if count eq 0 then begin
      message, 'SENSITIVITY: Measurement position not found'
    endif

;    case x['FUNCTION', xi] of
;      'X': denominator=x['X', xi]*x['PERTSIZE', xi]
;      'LN(X)': denominator=exp(x['X', xi])*x['PERTSIZE', xi]
;      '1/X': denominator=1.d/x['X', xi]*x['PERTSIZE', xi]
;      'LN(1/X)': denominator=1.d/exp(x['X', xi])*x['PERTSIZE', xi]
;    endcase
    denominator=x['PERTSIZE', xi]
    
    H[xi-xi_start, wh_y]=(y_pert[wh_y] - y['MODEL', wh_y])/denominator
    H_lifetime[xi-xi_start, wh_lifetime]=(out_lifetime[wh_lifetime] - lifetime['MODEL', wh_lifetime])/$
      denominator

  endfor

end