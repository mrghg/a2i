function a2i_sensitivity_parallel, parameters, y, model, x, lifetime, restart, $
  H_lifetime=H_lifetime, nProcesses=nProcesses, create_processes=create_processes, $
  destroy_processes=destroy_processes, oBridge=oBridge

  compile_opt idl2, hidden

  H=dblarr(X['NSTATE'], Y['NMEASUREMENTS'])
  H_lifetime=dblarr(X['NSTATE'], parameters['TIMESIZE']*parameters['NPOLLUTANTS'])
  
  
  ;Determine how many state elements per process
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  
  process_weight=intarr(x['NSTATE'])
  whP=where(strmid(x['ID'], 0, 1) eq 'P', countP, complement=whNP, nComplement=countNP)
  if countP gt 0 then begin
    for xi=0, countP-1 do begin
      if x['ID', whP[xi]] eq 'PO' then begin
        dummy=where(parameters['OH_A']*exp(parameters['OH_ER']/293.) gt 1.e-16, countOH)
        process_weight[whP[xi]]=countOH*(x['PERTEND', whP[xi]] - x['PERTSTART', whP[xi]])
      endif else begin
        process_weight[whP[xi]]=parameters['NPOLLUTANTS']*(x['PERTEND', whP[xi]] - x['PERTSTART', whP[xi]])
      endelse
    endfor
  endif
  if countNP gt 0 then begin
    process_weight[whNP]=x['PERTEND', whNP] - x['PERTSTART', whNP]
  endif

  process_weight_cumulative=total(process_weight, /cumulative)
  xi_per_process=float(process_weight_cumulative[-1])/float(nProcesses)

  xi_start=fltarr(nProcesses)
  xi_end=fltarr(nProcesses)
  
  wh=where(process_weight_cumulative lt xi_per_process)
  xi_start[0]=0
  xi_end[0]=wh[-1]
  for proI=1, nProcesses-1 do begin
    xi_start[proI]=xi_end[proI-1]+1
    wh=where(process_weight_cumulative lt (proI+1)*xi_per_process)
    xi_end[proI]=wh[-1]
  endfor
  xi_end[nProcesses-1]=x['NSTATE']-1
  

  ;Initiate IDL bridge processes
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;  oBridge=hash()
  if n_elements(oBridge) eq 0 then begin
    oBridge=!null
  endif
  
  For proI=0, nProcesses-1 do begin
    if create_processes then begin
      oBridge=[oBridge, obj_new("IDL_IDLBridge", output='')]
;    oBridge[proI]=obj_new("IDL_IDLBridge", output='')
      oBridge[proI]->execute, '@' + PREF_GET('IDL_STARTUP')
    endif
    mr_hash_pass, parameters, oBridge[proI]
    mr_hash_pass, y, oBridge[proI]
    mr_hash_pass, model, oBridge[proI]
    mr_hash_pass, x, oBridge[proI]
    mr_hash_pass, lifetime, oBridge[proI]
    mr_hash_pass, restart, oBridge[proI]
    oBridge[proI]->setvar, 'xi_start', xi_start[proI]
    oBridge[proI]->setvar, 'xi_end', xi_end[proI]
  endfor
  
  ;Run processes
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  For proI=0, nProcesses-1 do begin
    oBridge[proI]->execute, strcompress('a2i_sensitivity_parallel_run, parameters, y, model, x, lifetime, restart, ' + $
      'xi_start, xi_end, H, H_lifetime, error_check'), /nowait
  endfor
  

  ;Wait until all complete
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  allDone=0
  while allDone eq 0 do begin
    fins=0
    for proI=0, nProcesses-1 do fins+=(obridge[proI]->status() ne 1)
    if fins ge nProcesses then alldone=1 else wait, 1.
  endWhile


  ;Get output
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  for proI=0, nProcesses-1 do begin
    error_check=oBridge[proI]->getvar('error_check')
    if error_check ne 'no probs' then begin
      message, error_check
    endif
    H[xi_start[proI]:xi_end[proI], *]=oBridge[proI]->getvar('H')
    H_lifetime[xi_start[proI]:xi_end[proI], *]=oBridge[proI]->getvar('H_lifetime')
  endfor


  ;Destroy processes
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  if destroy_processes then begin
    for proI=0, nProcesses-1 do begin
      oBridge[proI].Cleanup
    endfor

;    ;Destroy timer info (https://groups.google.com/forum/#!topic/comp.lang.idl-pvwave/J2QMUUvmPLY)
;    timer_id=widget_info(/managed)
;    ww=where(widget_info(timer_id,/event_pro) eq 'IDL_IDLBRIDGETIMER_EVENT',nn_w)
;    timer_id=timer_id[ww[nn_w-1]]
;    widget_control, timer_id, /destroy

  endif

  return, H
  

end