; docformat = 'rst'
;
;+
;
; :Purpose:
;   Generate box model input fields
;
; :Inputs:
;   Parameters: hash table containing simulation parameters
;   Measurements: hash table containing measurement parameters
;   Emissions: hash table containing emissions information
;
; :Outputs:
;   Hash table containing model input parameters
;
; :Example::
;   
;   model=a2i_model(parameters, measurements, emissions)
;
; :History:
; 	Written by: Matt Rigby, University of Bristol, Jul 15, 2013
;
;-
function a2i_model, parameters, measurements, emissions

  compile_opt idl2, hidden


  ;Transport parameters
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;  
  if parameters['IAV'] then begin
    T_scale=replicate(1.d, 17, 12*parameters['NYEARS'])
  endif else begin
    T_scale=replicate(1.d, 17, 12)
  endelse

  ;OH
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  OH_scale=replicate(1.d, 8, 12*parameters['NYEARS'])

  ;Species-specific model parameters
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  
  modelLifetime=hash()
  modelEmissions=hash()
  
  For pi=0, parameters['NPOLLUTANTS']-1 do begin
    
    lifetime=replicate(1.e12, 12, (parameters['TIMESIZE'] - measurements['START', pi])/3L)

    ;Stratospheric lifetime
    lifetime=a2i_model_lifetime_stratosphere(parameters['STRATOSPHERIC_LIFETIME', pi], lifetime)

    ;Ocean lifetime
    lifetime=a2i_model_lifetime_ocean(parameters['OCEAN_LIFETIME', pi], lifetime)

    ;Tropospheric lifetime
    lifetime=a2i_model_lifetime_troposphere(parameters['TROPOSPHERIC_LIFETIME', pi], lifetime)    

    ;Emissions
    modelEmissions[pi]=emissions['EMISSIONS', pi, *, *]
    modelLifetime[pi]=lifetime
    
  endfor

  ;Initial conditions
  ic=a2i_initial_conditions(parameters, measurements, emissions)

  output=hash()
  output['EMISSIONS']=modelEmissions
  output['LIFETIME']=modelLifetime
  output['T_SCALE']=T_Scale
  output['OH_SCALE']=OH_Scale
  output['IC']=IC

  return, output
  
end