function a2i_model_lifetime_stratosphere, stratospheric_lifetime, lifetime

  if stratospheric_lifetime eq 0. then begin
    stratLife=1.e12
  endif else begin
    stratLife=stratospheric_lifetime
  endelse

  ;Apply relative lifetime scaling
  restore, mr_filestr('/Programs/A2I/strat_invlifetime_relative.sav')
  lifetime_relative=rebin(invLifetime_relative, $
    [n_elements(invLifetime_relative[*, 0]), $
    n_elements(invLifetime_relative[0, *])/3])
  for yi=0, n_elements(lifetime[0, *])/4-1 do begin
    lifetime[8:11, yi*4:(yi+1)*4-1]=1./((1./stratLife)*lifetime_relative)
  endfor

  return, lifetime

end