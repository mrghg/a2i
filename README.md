AGAGE 2D Inversion (A2I) framework

This code is to be used with the AGAGE 12-box model to infer emissions based on atmospheric observations. You will require:

- IDL 8 or higher
- The AGAGE 12-box model
- Matt Rigby's IDL library

There isn't much documentation at present. Contact Matt Rigby, University of Bristol, if you'd like to use.

Please note that this code is only for collaborative use. Please contact me if you would like to use the outputs from this code in any publications.