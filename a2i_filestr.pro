; docformat = 'rst'
;
; NAME:
;   a2i_fileStr
;
; PURPOSE:
;   Append user-specific input or output directory to front of file or directory string.
;   Also to correct slash/backslash for unix/windows systems
;
;+
; :Categories:
;   File system
;
; :Returns:
;   File string formatted for operating system, with input/output directory prefixed
;
; :Requires:
;   a2i_path.pro batch file containing two lines::
;     input_directory={input_directory}
;     output_directory={output_directory}
;
; :Params:
;   Ext : string variable containing file/directory to which directory prefix should be prefixed
;
; :Keywords:
;   Input: (input, Boolean) Use input_directory path specified in a2i_path.pro batch file
;   Output: (input, Boolean) Use Output_directory path specified in a2i_path.pro batch file
;
; :Examples::
; 
;    IDL> full_path=a2i_filestr(/Input, '/dir/filename')
;    
;    IDL> print, full_path
;    
;       /My/input/path/dir/filename
;
; :History:
;   Written by: Matt Rigby, University of Bristol, Aug 1, 2012
;
;-
function a2i_filestr, ext, Input=Input, Output=Output

  compile_opt idl2, hidden
  on_error, 2

  if keyword_set(Input) eq 0 and keyword_set(Output) eq 0 then begin
    message, 'Specify Input or Output keyword'
  endif
  
  ;Get input_directory and output_directory from a2i_path.pro
  @a2i_path
  
  if keyword_set(Input) then begin
    filestr=Strcompress(Input_directory + ext, /remove_all)  
  endif
  if keyword_set(Output) then begin
    filestr=Strcompress(Output_directory + ext, /remove_all)  
  endif
  
  ;Change filename for consistency with operating system, if required
  if !version.os_family eq 'Windows' then begin
    pos=strpos(filestr, '/')
    If pos ne -1 then begin
      while pos ne -1 do begin
        strput, filestr, '\', pos
        pos=strpos(filestr, '/')
      Endwhile
    Endif
  endif else begin
    pos=strpos(filestr, '\')
    If pos ne -1 then begin
      while pos ne -1 do begin
        strput, filestr, '/', pos
        pos=strpos(filestr, '\')
      Endwhile
    Endif
  Endelse

  return, filestr

End