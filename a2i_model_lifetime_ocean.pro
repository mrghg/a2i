function a2i_model_lifetime_ocean, ocean_lifetime, lifetime

  if ocean_lifetime eq 0 then begin
    ;Do nothing
  endif else begin
    oceanLife=ocean_lifetime
    oceanLife1=1./oceanLife
;    so_frac=0.4
;    latitude_scaling=[replicate(1. - so_frac, 3)/3., so_frac]
;    latitude_scaling=[0.152634961, $
;      0.117287918, $
;      0.27779563, $
;      0.452281491]

    latitude_scaling=[0.18349867, $
      0.154188985, $
      0.287281601, $
      0.375030744]

    for ti=0, n_elements(lifeTime[0, *])-1 do begin
      lifeTime[0:3, ti]=1./(latitude_scaling*oceanLife1 + 1./lifeTime[0:3, ti])
    endfor
  endelse

  return, lifetime

end