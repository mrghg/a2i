function a2i_model_lifetime_troposphere, tropospheric_lifetime, lifetime

  if tropospheric_lifetime eq 0 then begin
    tropLife=replicate(1.e12, 8, 12)
  endif else begin
    tropLife=tropospheric_lifetime
    ;Apply scaling based on OH concentration
    restore, file_which('agage_box_model_input_oh.sav')
    tropLife_scaling=1./OH[0:7, *]
    tropLife_scaling=tropLife_scaling/mean(tropLife_scaling, /nan)
    tropLife=tropLife*tropLife_scaling
  endelse
  tropLife=rebin(tropLife, 8, 4)
  for yi=0, n_elements(lifetime[0, *])/4-1 do begin
    lifeTime[0:7, yi*4:(yi+1)*4-1]=1./(1./tropLife + 1./lifetime[0:7, yi*4:(yi+1)*4-1])
  endfor

  return, lifetime

end