function a2i_model_scale, parameters, measurements, model, x

  compile_opt idl2, hidden

  model_Scale=mr_hash_copy(model)

  x_id=x['ID']
  x_ti=x['TI']
  x_box=x['BOX']
  x_pollutant=x['POLLUTANT']
  x_function=x['FUNCTION']
  
  y_start=measurements['START']
  nYears_species=parameters['TIMESIZE'] - y_start

  xiMin=0
  xiMax=X['NSTATE']-1
  x_adjust=x['X']

  ;Time periods for each parameter
  if parameters['IAV'] then begin
    tDt=3L
  endif else begin
    tDt=1L
  endelse
  oDt=3L
  qDt=12L/parameters['EMISSIONS_FREQUENCY']

  ;Go through each state element
  for xi=xiMin, xiMax do begin

    ;Find correct function
    case x_function[xi] of
      'X': xt=x_adjust[xi]
      '1/X': xt=1.d/x_adjust[xi]
      'LN(1/X)': xt=1.d/exp(x_adjust[xi])
      'LN(X)': xt=exp(x_adjust[xi])
    endcase

    case x_id[xi] of
      'I': begin
        model_scale['IC', x_pollutant[xi], x_box[xi]]=$
          model_scale['IC', x_pollutant[xi], x_box[xi]]*xt[0]
      end
      'Q': begin
        model_scale['EMISSIONS', x_pollutant[xi], x_box[xi], x_ti[xi]-y_start[x_pollutant[xi]]:x_ti[xi]-y_start[x_pollutant[xi]]+qDt[x_pollutant[xi]]-1]=$
          model_scale['EMISSIONS', x_pollutant[xi], x_box[xi], x_ti[xi]-y_start[x_pollutant[xi]]:x_ti[xi]-y_start[x_pollutant[xi]]+qDt[x_pollutant[xi]]-1]*xt[0]
      end
      'S': begin
        season=(x['PERTSTART', xi] - y_start[x_pollutant[xi]])/3L

        if x_box[xi] gt 0 then begin
          boxes=[x_box[xi], x_box[xi]]
          timeSteps=x['PERTSTART', $
            where(x['ID'] eq 'S' and x['POLLUTANT'] eq x_pollutant[xi] and x['BOX'] eq x_box[xi], $
            count)]
        endif else begin
          boxes=[8, 11]
          timeSteps=x['PERTSTART', $
            where(x['ID'] eq 'S' and x['POLLUTANT'] eq x_pollutant[xi], $
            count)]
        endelse

        if x_ti[xi] eq y_start[x_pollutant[xi]] then begin
          if count eq 1 then begin
            nSeasons=n_elements(model_scale['LIFETIME', x_pollutant[xi], boxes[0], *])
          endif else begin
            nSeasons=(timeSteps[1]-timeSteps[0])/3
          endelse
        endif else begin
          nSeasons=parameters['STRATOSPHERIC_LIFETIME_FREQUENCY', x_pollutant[xi]]*12L/3L
        endelse

        model_scale['LIFETIME', x_pollutant[xi], boxes[0]:boxes[1], season : season+nSeasons-1]=$
          model_scale['LIFETIME', x_pollutant[xi], boxes[0]:boxes[1], season : season+nSeasons-1]*xt[0]

      end
      'PT': begin
        if x_ti[xi] lt 0 then begin
          model_scale['T_SCALE', x_box[xi], -1*(x_ti[xi])-1 : -1*(x_ti[xi])-1 + tDt - 1 ]=$
            model_scale['T_SCALE', x_box[xi], -1*(x_ti[xi])-1 : -1*(x_ti[xi])-1 + tDt - 1 ]*xt[0]
        endif else begin
          model_scale['T_SCALE', x_box[xi], x_ti[xi] : x_ti[xi] + tDt - 1 ]=$
            model_scale['T_SCALE', x_box[xi], x_ti[xi] : x_ti[xi] + tDt - 1 ]*xt[0]
        endelse
      end
      'PO': begin
;        model_scale['OH_SCALE', [x_box[xi], x_box[xi]+4], x_ti[xi] : x_ti[xi] + oDt - 1 ]=$
;          model_scale['OH_SCALE', [x_box[xi], x_box[xi]+4], x_ti[xi] : x_ti[xi] + oDt - 1 ]*xt[0]
        model_scale['OH_SCALE', x_box[xi], x_ti[xi] : x_ti[xi] + oDt - 1 ]=$
          model_scale['OH_SCALE', x_box[xi], x_ti[xi] : x_ti[xi] + oDt - 1 ]*xt[0]
      end
    endcase
    
  endfor

  return, model_scale

end