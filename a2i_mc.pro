pro a2i_mc, parameters, y, x, model, mc=mc

  if keyword_set(mc) eq 0 then begin
    seed=systime(/seconds)
  endif else begin
    seed=mc
  endelse

;  case strUpCase(parameters['FUNCTION_Y']) of
;    'LN(Y)': begin
;      y['Y']=alog( exp(y['Y']) * (1. + 0.02*randomn(seed)) )
;    end
;    'Y': y['Y']=y['Y']*(1. + 0.02*randomn(seed))
;  endcase

  wh=where(strMid(strUpCase(x['FUNCTION']), 0, 2) eq 'LN', count, complement=whC)
  if count gt 0 then x['AP', wh]=x['ERROR', wh]*randomn(seed, count, /double)
  x['AP', whC]=x['AP', whC]*(1. + x['ERROR', whC]*randomn(seed, n_elements(whC), /double))

  x['X']=x['AP']

  model=a2i_model_scale(parameters, y, model, x)
  
end